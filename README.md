# Scheduled Task to Monitor Application Health with Python

#### Project Outline

In this project we will monitor a website running on a cloud server, we will use Linode in this specific case

Our website will contain the following

1-	A server on a Linode Cloud Platform

2-	Docker container running on the server

3-	Run nginx on the container

In our script we will include the below

4-	Checks the application by checking the status through status code requests and responses

5-	Sends an email, when website is down

6-	Can then include a fix in the script which will restart the Docker container and Linode server

7-  Will also implement a schedule for the script run at a specific period of time

So in summary our python script will monitor the website and fix the issues that occur within the server and the application

#### Lets get started

First step will be to create a Linode server and install docker and run a nginx container

Lets create our Linode server

![Image1](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image1.png)

Can then add ssh key

![Image2](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image2.png)

```
cat ~/.ssh/id_rsa.pub
```

![Image3](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image3.png)

And then add it

![Image4](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image4.png)

Now ready to create

![Image5](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image5.png)

And now running

![Image6](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image6.png)

Can now ssh into it

```
ssh root@212.71.239.218
```

Now able to ssh in


![Image7](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image7.png)

Now ready to install docker on Debian, can use the below link

https://docs.docker.com/engine/install/debian/

Can now install it using the steps

![Image8](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image8.png)

Can see docker is now installed

```
docker –version
```

![Image9](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image9.png)

So, this command starts an Nginx server within a Docker container, making it accessible on port 8080 of the host machine while the actual Nginx service inside the container listens on its default port 80.

```
docker run -d -p 8080:80 nginx
```

![Image10](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image10.png)

Now using the public ip address and the port can access application

![Image11](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image11.png)

Able to now access the application

```
http://212.71.239.218:8080/
```

![Image12](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image12.png)

Now that we have prepared our server we can create our automation script


Lets create a new file

```
monitor-website.py
```

![Image13](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image13.png)

Since we will be making http requests we can use a library called request and install it using pip

```
pip install requests
```

And then import in our file so that we can use the library

![Image14](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image14.png)

Also note we can use the DNS name to access the web

![Image15](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image15.png)

![Image16](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image16.png)

Can the write a code which prints out the response, in this case it is 200 where we can make successfull requests

![Image17](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image17.png)

Using the below reference 

![Image18](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image18.png)

Can then run our code to get the status code and returns ‘200’ and therefore successful


![Image19](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image19.png)

Can also implement an if statement

![Image20](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image20.png)

Now lets configure the email configuration if the website were to go down

Can implement smtplib library to start sending emails

![Image21](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image21.png)

In this project we will use g-mail, to allow our python script to connect to gmail and send the email

Can use gmail and port 587

![Image22](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image22.png)

Calling smptp variable

Call SMTP function to configure which email provider you want to use and insert the configuration by putting it in parameters the email provider and the port its accessible on

Use with statement which is used with unmanaged resourcees used in exception handling and clean up code to make code cleaner

as smtp mean is that we assign all of what is before it to a variable

![Image23](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image23.png)

Call function starttls on smtp which will encrypt the communication from python to our email server

Adding ehlo

Call function ehlo which will identify python application with mail server

![Image24](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image24.png)

Call login function to login into our account using the password generate in the below

![Image25](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image25.png)

Can then configure the below

![Image26](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image26.png)

It is not secure to have your password exposed in python file that is going to be pushed to a repo so its better for us to have access to this credentials through environmental variables set on our local computer, to do this we will need to import os module


In order to set the environment variables locally in the editor can navigate to the below

![Image29](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image29.png)

Can then add the values

![Image30](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image30.png)

Can then import os library and insert the email variables to login

![Image27](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image27.png)

![Image28](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image28.png)

Now that we have the necessary parameters we can now send to email address, as well as the recipient and the subject

Can now sendemail function and put sender, receiver and email message in the parameter and implement it in our code

Can also set the message as a variable

![Image31](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image31.png)

Can then test the code by setting to false as a quick check and see it is successful

![Image32](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image32.png)

And can see the email is now sent

![Image33](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image33.png)

Now we have to implement logic is not just a status code other than 200 and is a result of perhaps the linode server is down or there is a connection issue and therefore throws an exception. If that happens we will definitely want to get an email

In that case we will use try/except method 

Lets see what happens when we stop the container

![Image34](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image34.png)

And then try running the script again

And then placing Exception as ‘ex’

The Exception class in Python captures a wide range of errors that might occur during code execution. In the given code, using except Exception as ex: allows handling various potential issues, like network errors in HTTP requests or problems while sending emails, in a general way

Can then run it

And it will display the error

![Image35](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image35.png)

Further look at the exception

![Image36](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image36.png)

Can then configure the code to do two things, return the status code if it is not 200, if it does not return even a status code can include the code to return an exception which will send an email

Also included the other status code as an f string

![Image37](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image37.png)

Now looking at the try and except functions they are both the same, so to stop repeating can create a function, this block of code using smtplib performs SMTP actions to establish a secure connection to a Gmail SMTP server, authenticate using provided credentials, and send a notification email from and to the same email address to indicate an issue with the application's accessibility.


Can then configure the function and then call it in the try function

![Image38](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image38.png)

But for the ‘Exception’


Can first configure it in the try statement

![Image39](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image39.png)

Can then do it with exception

![Image40](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image40.png)

Fixing the warning

![Image41](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image41.png)

Further completed code

Can now execute it aswell

![Image42](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image42.png)

And can see the email notification worked

![Image43](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image43.png)

Now that we have in our logic that the application is now down, we now want to repair the application, when the application returns a status code other than 200, to fix that we can implement logic to restart the application which in our case is the nginx docker container

Lets write our logic to restart the application

The way to begin is to Connect to our Linode server using SSH with our Python script and the execute docker start command. To enable our python script to make ssh connections we can use a library called Paramiko using the below documentation

Using the below documentation

https://pypi.org/project/paramiko/

![Image44](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image44.png)

Can then install it in our terminal

```
pip install paramiko
```

![Image45](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image45.png)

Can then import the module

![Image46](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image46.png)

Upon configuring the function can see the list of parameter

![Image47](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image47.png)

Can start of with the host and username

![Image48](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image48.png)

Can then specify the private key which we added to our linode account

![Image49](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image49.png)

Can then start executing linux/docker commands

![Image50](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image50.png)

However we have to note, we want to see the outputs and we get three outputs which is stdin(input), stdout(output) and stderr(error) and configure it like the below

![Image51](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image51.png)

When connecting to a host the first time we are prompted to say yes or no

Can add this piece of line to auto approve

Aswell as to print the stdin and stdout

![Image52](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image52.png)

Can also remove the print stdin and stdout and make stdout.readlines()

Now ready to execute the below script

![Image53](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image53.png)

Scrolling further to the left of the terminal logs can see the information about our nginx container since we restarted it manually

![Image54](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image54.png)

Once we are done with connection can do ssh close

![Image55](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image55.png)

Once all configured can restart the docker container with it’s harcoded ID and the printout that the application/docker container has been restarted

![Image56](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image56.png)

Lets say the issue is with the server and not getting a status code response, we can include logic in the exception to restart the server and the application

![Image57](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image57.png)

Lets utilise our python script to connect to our linode account

We can use the below library

![Image58](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image58.png)

```
pip install linode-api4
```

And can see it has successfully installed

![Image59](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image59.png)

Can then import the library

![Image60](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image60.png)

And then setup the client

![Image61](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image61.png)

In order for our Python script to connect to our Linode account we need an established connection just like we did with the SSH, with Linode we will use the API token

![Image62](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image62.png)

Can create the token and place it in the client

![Image63](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image63.png)

However for security reasons we want to set it as a variable using the editor like we did for the email credentials

Can then also set it

![Image64](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image64.png)

Using the below link

https://linode-api4.readthedocs.io/en/latest/guides/getting_started.html#:~:text=Installation&text=If%20you%20prefer%2C%20you%20can,linode_api4%20python%20%2Dm%20pip%20install%20


We can now set the client with our API token

![Image65](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image65.png)

Can also specify the parameters which are also target types and id’s, can then include re-boot function to re-boot our linode server

Just like the below

![Image66](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image66.png)

Let’s test and stop the docker container

![Image67](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image67.png)

Lets then run the script again

![Image68](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image68.png)

Can see the connection closed since it is rebooting

![Image69](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image69.png)

Now let’s implement logic to restart the docker container


Can then put it in to a function since it is getting used more than once

![Image70](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image70.png)

Since it is being called in restart the application and the exception, we can now call it

Just like the below

![Image71](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image71.png)

Now we may have a timing issue, where it will try and restart the application before the server is up and running

Can try the below and create a while loop so that the server is in a running state we can then execute the docker restart container and then stop/break the loop so that it doesn’t run indefinitely

![Image72](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image72.png)

Can now execute

![Image73](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image73.png)

Now checking can see the docker container restarted

![Image74](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image74.png)

Even though the server is running it may not be fully initialized, we can implement a time interval before we can run some operational commands on it 

Can import time

![Image75](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image75.png)

Can them implement 5 seconds rule

![Image76](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image76.png)

To clean up our code, we can put the restart linode server and application in to one function

![Image77](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image77.png)

Can then call upon it

![Image78](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image78.png)

Now we want this to run at different time intervals, so to proceed we can put both restart container and restart server and container functions in to one function

Just like the below

![Image79](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image79.png)

Can then put in logic for this to run every five minutes and call the function ‘monitor_application()’

![Image80](https://gitlab.com/FM1995/scheduled-task-to-monitor-application-health-with-python/-/raw/main/Images/Image80.png)















































